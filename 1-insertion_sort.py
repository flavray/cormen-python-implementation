def insertion_sort(A):
    for j, key in enumerate(A[1:], start=1):
        i = j - 1
        while i >= 0 and A[i] > key:
            A[i+1] = A[i]
            i -= 1
        A[i+1] = key

def non_increasing_insertion_sort(A):
    for j, key in enumerate(A[1:], start=1):
        i = j - 1
        while i >= 0 and A[i] < key:
            A[i+1] = A[i]
            i -= 1
        A[i+1] = key

def linear_search(A, v):
    for index, value in enumerate(A):
        if v == value:
            return index
    else:
        return None

def binary_addition(n, A, B):
    # Addition rules. key : bits to add + previous carry over, value : (result, carry_over)
    rules = {
        (0, 0, 0): (0, 0), (0, 0, 1): (1, 0),
        (0, 1, 0): (1, 0), (0, 1, 1): (1, 0),
        (1, 0, 0): (1, 0), (1, 0, 1): (1, 0),
        (1, 1, 0): (0, 1), (1, 1, 1): (1, 1)
    }
	
    C = [0] * (n + 1)
    carry_over = 0

    for index in reversed(xrange(n)):
        a, b = A[index], B[index]
        C[index+1], carry_over = rules[(a, b, carry_over)]
    C[0] = carry_over

    return C


# Testing part #

if __name__ == '__main__':
    A = [5, 2, 4, 6, 1, 3]

    # [1, 2, 3, 4, 5, 6]
    B = list(A)
    insertion_sort(B)
    print B

    # [6, 5, 4, 3, 2, 1]
    B = list(A)
    non_increasing_insertion_sort(B)
    print B

    # 5
    print linear_search(A, 3)

    # None
    print linear_search(A, 42)

    # 3 + 10 = 13. [0, 1, 1, 0, 1]
    print binary_addition(4, [0, 0, 1, 1], [1, 0, 1, 0])

    # 7 + 4 = 11. [1, 0, 1, 1]
    print binary_addition(3, [1, 1, 1], [1, 0, 0])

	