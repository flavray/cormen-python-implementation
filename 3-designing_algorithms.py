def merge(A, p, q, r):
    L = A[p:q] + [float('inf')]
    R = A[q:r] + [float('inf')]

    i, j = 0, 0
    for k in xrange(p, r):
        if L[i] <= R[j]:
            A[k], i = L[i], i + 1
        else:
            A[k], j = R[j], j + 1

def other_merge(A, p, q, r):
    L = A[p:q]
    R = A[q:r]

    k = p
    while L and R:
        if L[0] <= R[0]:
            A[k] = L.pop(0)
        else:
            A[k] = R.pop(0)
        k += 1

    if L:
        A[k:r] = L
    else:
        A[k:r] = R

def merge_sort(A, p, r):
    if r - p > 1: # List size is greater than 1
        q = (p + r) / 2
        merge_sort(A, p, q)
        merge_sort(A, q, r)
        merge(A, p, q, r)

def binary_search(v, A, p, r):
    if p < r:
        q = (p + r) / 2
        if A[q] > v:
            return binary_search(v, A, p, q)
        elif A[q] < v:
            return binary_search(v, A, q+1, r)
        else:
            return q

def sub_sum(A, x):
    B = list(A)
    merge_sort(B, 0, len(B))

    i, j = 0, len(B) - 1
    while i <= j:
        if x > B[i] + B[j]:
            i = i + 1
        elif x < B[i] + B[j]:
            j = j - 1
        else:
            return True
    	
    return False

def insertion_sort(A, p, r):
    for j, key in enumerate(A[p+1:r], start=p+1):
        i = j - 1
        while i >= p and A[i] > key:
            A[i+1] = A[i]
            i -= 1
        A[i+1] = key

def merge_insertion_sort(A, p, r, k):
    if r - p > k:
        q = (p + r) / 2
        merge_insertion_sort(A, p, q, k)
        merge_insertion_sort(A, q, r, k)
        merge(A, p, q, r)
    elif r - p > 1:
        insertion_sort(A, p, r)

def bubble_sort(A):
    for i in xrange(len(A)):
        for j in reversed(xrange(i, len(A))):
            if A[j] < A[j-1]:
                A[j], A[j-1] = A[j-1], A[j]

def horner(A, x):
    return reduce(lambda y, a: a + x * y, A)

if __name__ == '__main__':
    A = [5, 2, 4, 6, 1, 3]

    B = list(A)
    merge_sort(B, 0, len(B))
    print B

    print binary_search(3, A, 0, len(A))
    print binary_search(42, A, 0, len(A))

    print sub_sum(A, 6)
    print sub_sum(A, 42)

    B = list(A)
    merge_insertion_sort(B, 0, len(B), 2)
    print B

    B = list(A)
    bubble_sort(B)
    print B

    # 2 ** 2 - 2 + 1 = 3
    print horner([1, -1, 1], 2)
