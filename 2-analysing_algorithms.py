def selection_sort(A):
    for i in xrange(len(A)):
        min_index = i
        for j, value in enumerate(A[i:], start=i):
            if value < A[min_index]:
                min_index = j
        A[i], A[min_index] = A[min_index], A[i]

if __name__ == '__main__':
    A = [5, 2, 4, 6, 1, 3]

    selection_sort(A)
    print A
